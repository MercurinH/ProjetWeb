<?php
//include "_inc.php"; // on charge les paramètres bdd

date_default_timezone_set('Europe/Paris');
try{
  // le fichier de BD s'appellera contacts.sqlite
  $file_db=new PDO('sqlite:./Films/film.sqlite');
  $file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
  $requetes="";

  $sql=file("./Films/genres.sql"); // on charge le fichier SQL
  foreach($sql as $l){ // on le lit
  	if (substr(trim($l),0,2)!="--"){ // suppression des commentaires
  		$requetes .= $l;
  	}
  }

  $reqs = explode(";",$requetes);// on sépare les requêtes
  foreach($reqs as $req){	// et on les éxécute
  	if (!sqlite_query($file_db, $req) && trim($req)!=""){
  		die("ERROR : ".$req); // stop si erreur
  	}
  }
  echo "base restaurée";
  echo "Insertion en base reussie !";
  // on ferme la connexion
  $file_db=null;
}
catch(PDOException $ex){
  echo $ex->getMessage();
}
?>
