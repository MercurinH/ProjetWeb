<!doctype html>
<html lang='fr'>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="recherche.css">
<title>Recherche de film</title>
<style>  input[type="range"] {
    position: relative;
    margin-left: 1em;
      }
      input[type="range"]:after,input[type="range"]:before {
        position: absolute;
        top: 1em;
        color: #aaa;
      }
      input[type="range"]:before {
        left:0em;
        content: attr(min);
      }
      input[type="range"]:after {
        right: 0em;
        content: attr(max);
      }
</style>
</head>
<body>

<section></section>
<img src="logo.png" alt="logo" style="width:250px;height:250px;">
<section></section>
<h1>
	Recherchez votre film:
</h1>
<div>
<form name="search" method="GET" action="./resultat.php">
</br>
  <p>
      <label>Recherche par nom</label> : <input type="text" value="nom" />
  <p>
      <label>Recherche par genre</label> : <input type="text" name="genre" />
  </p>
  <p>
      <label>Recherche par année</label> : <input type="int" name="année" />
  </p>
  <p>
      <label>Recherche par réalisateur</label> : <input type="text" name="réalisateur" />
  </p>
<br/>
<p><input type="submit" value ="Recherche"></p>
<br/>
<p><input type="reset" value="Remise à zéro"></p>
</form>
</div>
</body>
</html>
