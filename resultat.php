<link rel="stylesheet" href="recherche.css">
<?php
date_default_timezone_set('Europe/Paris');
try{
  // le fichier de BD s'appellera contacts.sqlite
  $file_db=new PDO('sqlite:./Films/film.sqlite');
  $file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);

// recherche par nom
  if ((!empty($_GET['nom']) and (empty($_GET['genre']))
    and (empty($_GET['année'])) and (empty($_GET['réalisateur'])))){
      $result=$file_db->query('Select distinct * from films where films.titre_original=$_GET["nom"] or films.titre_francais=$_GET["nom"]');
      echo "<ul>\n";
      foreach ($result as $film){
        echo "<li>".$film['titre_original'].$film['annee'].
      "</li>\n";
      }
    }
// recherche par genre
  elseif (((empty($_GET['nom']) and (!empty($_GET['genre']))
    and (empty($_GET['année'])) and (empty($_GET['réalisateur']))))){
      $result=$file_db->query("Select distinct * from films natural join classification natural join genres
       where genres.nom_genre=\'$genre\' and genres.code_genre=classification.ref_code_genre and classification.ref_code_film=films.code_film");
      echo "<ul>\n";

      foreach ($result as $film){
        echo "<li>".$film['titre_original'].$film['annee'].
      "</li>\n";
      }
    }
  // recherche par année
  elseif (((empty($_GET['nom']) and (empty($_GET['genre']))
    and (!empty($_GET['année'])) and (empty($_GET['réalisateur']))))){
      $result=$file_db->query("Select distinct * from films
       where films.date=\'$annee\'");
      echo "<ul>\n";

      foreach ($result as $film){
        echo "<li>".$film['titre_original'].$film['annee'].
      "</li>\n";
      }
    }
    // recherche par realisateur
  elseif (((empty($_GET['nom']) and (empty($_GET['genre']))
      and (empty($_GET['année'])) and (!empty($_GET['réalisateur']))))){
        $result=$file_db->query("Select distinct * from films natural join individus
         where individus.nom=\'$real\' and individus.code_indiv=films.realisateur");
        echo "<ul>\n";

        foreach ($result as $film){
          echo "<li>".$film['titre_original'].$film['annee'].
        "</li>\n";
      }
    }
// Affiche tout les films
  elseif (((empty($_GET['nom'])) and (empty($_GET['genre']))
      and (empty($_GET['année'])) and (empty($_GET['réalisateur'])))){
        $result=$file_db->query('Select * from films');
        echo "<ul>\n";
        foreach ($result as $film){
          echo "<li>".$film['titre_original'].$film['titre_francais'].$film['pays'].$film['date'].
          "</li>\n";
      }
  foreach ($result as $film){
    echo "<li>".$film['titre_original'].$film['annee'].
  "</li>\n";
  }
}
  echo "</ul>";

  $file_db=null;
}
catch(PDOException $ex){
  echo $ex->getMessage();
}
?>
